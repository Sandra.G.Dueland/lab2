package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

		public ArrayList<FridgeItem> fridge;
		public int counter;
		public final int maxSize = 20;
	
	public Fridge() {
		this.fridge = new ArrayList<FridgeItem>();
		this.counter = 0;
	}
	
	@Override
	public int nItemsInFridge() {
		return counter;
	}
	
	@Override
	public int totalSize() {
		return maxSize;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (counter<maxSize) {
			fridge.add(item);
			counter ++;
			return true;		
		}
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (!fridge.contains(item)){
			throw new NoSuchElementException();
		}
		else {
			fridge.remove(item);
			counter --;	
		}		
		
	}

	@Override
	public void emptyFridge() {
		fridge.clear();
		counter = 0;
		
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
		for (int i = 0; i < counter; i++) {
			if (fridge.get(i).hasExpired()) {
				expiredFood.add(fridge.get(i));
				takeOut(fridge.get(i));
				i--;
			}
		}
		
		return expiredFood;
	}

}
